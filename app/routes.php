<?php
// Routes

$app->map(['get', 'post'], '/', 'App\Controller\IndexController:indexAction')
    ->setName('homepage');

$app->group('/discount', function () {
    $this->get('', 'App\Controller\DiscountController:indexAction')
        ->setName('discount-index');
    $this->map(['get', 'post'], '/edit[/{id:[0-9]+}]', 'App\Controller\DiscountController:editAction')
        ->setName('discount-edit');
    $this->get('/delete/{id:[0-9]+}', 'App\Controller\DiscountController:deleteAction')
        ->setName('discount-delete');
});

$app->group('/service', function () {
    $this->get('', 'App\Controller\ServiceController:indexAction')
        ->setName('service-index');
    $this->map(['get', 'post'], '/edit[/{id:[0-9]+}]', 'App\Controller\ServiceController:editAction')
        ->setName('service-edit');
    $this->get('/delete/{id:[0-9]+}', 'App\Controller\ServiceController:deleteAction')
        ->setName('service-delete');
});