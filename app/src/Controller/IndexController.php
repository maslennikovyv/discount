<?php
namespace App\Controller;

use App\Model\Discount;
use App\Model\Service;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;


class IndexController extends BaseController
{

    public function indexAction(Request $request, Response $response, $args)
    {
        $amount = 0;

        if($request->isPost()) {
            $amount = (new Discount($this->db))->calculate($request->getParsedBody());
        }

        $this->view->render($response, 'index.twig', [
            'amount' => $amount,
            'params' => $request->getParsedBody(),
            'services' => (new Service($this->db))->fetchAll(),
        ]);

        return $response;
    }

}