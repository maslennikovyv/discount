<?php
namespace App\Controller;

use Interop\Container\ContainerInterface;

class BaseController
{
    /**
     * @var ContainerInterface
     */
    protected $ci;

    /**
     * @var \Aura\Sql\ExtendedPdo
     */
    protected $db;

    /**
     * @var \Slim\Views\Twig
     */
    protected $view;

    public function __construct(ContainerInterface $ci)
    {
        $this->ci = $ci;
        $this->db = $ci->get('db');
        $this->view = $ci->get('view');
        $this->init();
    }

    protected function init()
    {

    }
}