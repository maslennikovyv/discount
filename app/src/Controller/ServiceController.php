<?php
namespace App\Controller;

use App\Model\Service;


class ServiceController extends AdminController
{
    protected function init()
    {
        $this->setModel(new Service($this->db));
    }
}