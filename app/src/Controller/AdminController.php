<?php
namespace App\Controller;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;


class AdminController extends BaseController
{
    protected $entity = null;
    protected $form = null;
    protected $model = null;

    public function indexAction(Request $request, Response $response, $args)
    {
        $model = $this->getModel();
        $this->view->render($response, 'admin/index.twig', [
            'entity' => $this->getEntityName(),
            'rows' => $this->fetchAll($model),
        ]);
        return $response;
    }

    public function editAction(Request $request, Response $response, $args)
    {
        $id = isset($args['id']) ? $args['id'] : false;
        $model = $this->getModel();

        if ('POST' == $request->getMethod()) {
            $data = $request->getParsedBody();
            if ($id) {
                $model->update($data, $id);
            } else {
                $model->insert($data);
            }
            return $response->withStatus(302)->withHeader('Location', '/' . $this->getEntityName());
        } else {
            $data = $id ? $model->findOne($id) : [];
        }

        $template = $request->isXhr() ? $this->getEntityName() . '/form.twig' : 'admin/edit.twig';

        return $this->renderForm($response, $template, [
            'id' => $id,
            'data' => $data,
            'entity' => $this->getEntityName(),
        ]);
    }

    public function deleteAction(Request $request, Response $response, $args)
    {
        $model = $this->getModel();
        $model->delete($args['id']);

        return $response->withStatus(302)->withHeader('Location', '/' . $this->getEntityName());
    }

    protected function getEntityName()
    {
        if (null === $this->entity) {
            $this->entity = mb_strtolower(str_replace([__NAMESPACE__ . '\\', 'Controller'], '', get_called_class()));
        }
        return $this->entity;
    }

    protected function setModel($model = null)
    {
        if (null === $model) {
            $name = ucfirst($this->getEntityName()) . 'Model';
            if ($this->ci[$name]) {
                $this->model = $this->ci[$name];
            } else {
                throw new \Exception('Container has no model ' . $name);
            }
        } else {
            $this->model = $model;
        }

        return $this;
    }

    protected function getModel()
    {
        if (null === $this->model) {
            $this->setModel();
        }

        return $this->model;
    }

    protected function fetchAll($model, $select = null)
    {
        return $model->fetchAll($select);
    }

    protected function renderForm($response, $template, $data)
    {
        return $this->view->render($response, $template, $data);
    }

}