<?php
namespace App\Controller;

use App\Model\Discount;
use App\Model\Service;


class DiscountController extends AdminController
{
    protected function init()
    {
        $this->setModel(new Discount($this->db));
    }

    protected function fetchAll($model, $select = null)
    {
        return $model->raw_sql_just_for_fun();
    }

    protected function renderForm($response, $template, $data)
    {
        $data['services'] = (new Service($this->db))->fetchAll();
        return parent::renderForm($response, $template, $data);
    }

}