<?php
namespace App\Model;

class Service extends Base
{
    const TABLE = 'service';
    const PK = 'service_id';
}