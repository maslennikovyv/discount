<?php
namespace App\Model;

class Discount extends Base
{
    const TABLE = 'discount';
    const PK = 'discount_id';

    protected function cols($query, $data, $query_type = null)
    {
        switch ($query_type) {
            case self::SELECT:
                $data['array_to_json("service")'] = 'service_json'; // Шероховатости Ауры: ключ = значение, значение = ключ
                $data['to_char(date_from, \'DD.MM.YYYY\')'] = 'ru_date_from';
                $data['to_char(date_to, \'DD.MM.YYYY\')'] = 'ru_date_to';

                break;
            case self::INSERT:
            case self::UPDATE:
                $data['date_from'] = empty($data['date_from']) ? null : $this->ru_date_convert($data['date_from']);
                $data['date_to'] = empty($data['date_to']) ? null : $this->ru_date_convert($data['date_to']);
                $data['phone_ends'] = empty($data['phone_ends']) ? null : $data['phone_ends'];
                $data['amount'] = (int)$data['amount'];

                if (!empty($data['service']) && is_array($data['service'])) {
                    $data['service'] = '{' . implode(',', array_map('intval', $data['service'])) . '}';
                } else {
                    $data['service'] = null;
                }
                break;
        }

        return parent::cols($query, $data);
    }

    /**
     * @return array
     */
    public function raw_sql_just_for_fun()
    {
        return $this->pdo->fetchAll('SELECT d1.*, d2.services, to_char(d1.date_from, \'DD.MM.YYYY\') as ru_date_from, to_char(d1.date_to, \'DD.MM.YYYY\') as ru_date_to FROM discount d1 LEFT JOIN (
	SELECT d.discount_id, array_to_json(array_agg(service.title)) as services FROM (SELECT *, unnest(service) as service_id FROM discount) as d LEFT JOIN service USING (service_id) GROUP BY d.discount_id
) d2 USING (discount_id);');
    }

    /**
     * @param $data
     * @return int
     */
    public function calculate($data)
    {
        $discount = 0;

        $select = $this->select();

        // Услуги
        if (isset($data['service']) && is_array($data['service'])) {
            $select->where(sprintf('(service is null OR service <@ ARRAY[%s]::bigint[])', implode(',', array_map('intval', $data['service']))));
        } else {
            $select->where('service is null');
        }

        // Birthday
        $b = date_format(date_create_from_format('d.m.Y', $data['birthday']), 'z');
        $t = date_format(date_create(), 'z');

        $before = ($b >= $t && $b <= $t + 7);
        $after = ($b <= $t && $b >= $t - 7);

        if (!$before && !$after) {
            $select->where('birthday = \'whatever\'');
        } elseif ($before) {
            $select->where('birthday IN (\'whatever\', \'before\', \'always\')');
        } elseif ($after) {
            $select->where('birthday IN (\'whatever\', \'after\', \'always\')');
        }

        // Заполнен телефон
        if (empty($data['phone'])) {
            $select->where('has_phone IN (\'whatever\', \'empty\') AND phone_ends is null');
        } else {
            $select->where('has_phone IN (\'whatever\', \'exists\') AND (phone_ends is null OR phone_ends = ?)', mb_substr($data['phone'], -4));
        }

        // Время действия
        $select->where('(date_from is null OR date_from <= current_date) AND (date_to is null OR date_to >= current_date)');

        // Пол
        $select->where('(gender = \'whatever\' OR gender = ?)', $data['gender']);

        $select->orderBy(['amount desc']);
        $select->limit(1);


        $row = $this->fetch($select);
        if ($row) {
            $discount = $row['amount'];
        }

        return $discount;
    }
}