<?php
namespace App\Model;

use Aura\SqlQuery\QueryFactory;

class Base
{
    const TABLE = null;
    const PK = null;

    const SELECT = 1;
    const INSERT = 2;
    const UPDATE = 3;

    protected $pdo;
    protected $query_factory;

    public function __construct(\Aura\Sql\ExtendedPdo $pdo)
    {
        $this->pdo = $pdo;
    }

    protected function getQueryFactory()
    {
        if ($this->query_factory === null) {
            $this->query_factory = new QueryFactory('pgsql');
        }

        return $this->query_factory;
    }

    public function select()
    {
        return $this->cols($this->getQueryFactory()
            ->newSelect()
            ->from(static::TABLE), ['*'], self::SELECT);
    }

    public function insert($data)
    {
        $insert = $this->cols($this->getQueryFactory()
            ->newInsert()
            ->into(static::TABLE), $data, self::INSERT);

        $sth = $this->pdo->prepare($insert->getStatement());

        return $sth->execute($insert->getBindValues());
    }

    public function update($data, $where)
    {
        $update = $this->where($this->cols($this->getQueryFactory()
            ->newUpdate()
            ->table(static::TABLE), $data, self::UPDATE), $where);

        $sth = $this->pdo->prepare($update->getStatement());

        return $sth->execute($update->getBindValues());
    }

    public function delete($where)
    {
        $delete = $this->where(
            $this->getQueryFactory()
                ->newDelete()
                ->from(static::TABLE), $where);

        $sth = $this->pdo->prepare($delete->getStatement());

        return $sth->execute($delete->getBindValues());
    }

    public function findOne($where)
    {
        $select = $this->where($this->select(), $where);
        return $this->fetch($select);
    }

    public function fetch($select = null, $type = \PDO::FETCH_ASSOC)
    {
        if (null === $select) {
            $select = $this->select();
        }

        $sth = $this->pdo->prepare($select->getStatement());
        $sth->execute($select->getBindValues());

        return $sth->fetch($type);
    }

    public function fetchAll($select = null, $type = \PDO::FETCH_ASSOC)
    {
        if (null === $select) {
            $select = $this->select();
        }
        $sth = $this->pdo->prepare($select->getStatement());
        $sth->execute($select->getBindValues());

        return $sth->fetchAll($type);
    }

    /**
     * where hook
     *
     * @param $query
     * @param $where
     * @return mixed
     */
    protected function where($query, $where)
    {
        if ($where == (string)(int)$where) {
            $query->where(static::PK . ' = ?', $where);
        } else {
            $query->where($where);
        }
        return $query;
    }

    /**
     * cols hook
     *
     * @param $query
     * @param $data
     * @param null $query_type
     * @return mixed
     */
    protected function cols($query, $data, $query_type = null)
    {
        return $query->cols($data);
    }

    protected function ru_date_convert($data)
    {
        return date_format(date_create_from_format('j.m.Y', $data), 'Y-m-d');
    }

}