<?php

use Phinx\Migration\AbstractMigration;

class Initial extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {

    }

    public function up()
    {

        $sql = <<<EOT

DROP TABLE IF EXISTS "service";

CREATE TABLE "service"
(
  service_id bigserial NOT NULL,
  title character varying(255),
  CONSTRAINT service_pkey PRIMARY KEY (service_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE "service"
  OWNER TO "user";


DROP TABLE IF EXISTS discount;

DROP TYPE IF EXISTS discount_gender;
CREATE TYPE discount_gender AS ENUM ('whatever', 'm', 'f');

DROP TYPE IF EXISTS discount_phone;
CREATE TYPE discount_phone AS ENUM ('whatever', 'exists', 'empty');

DROP TYPE IF EXISTS discount_birthday;
CREATE TYPE discount_birthday AS ENUM ('whatever', 'before', 'after', 'always');

CREATE TABLE discount
(
  discount_id bigserial NOT NULL,
  gender discount_gender NOT NULL DEFAULT 'whatever'::discount_gender,
  birthday discount_birthday NOT NULL DEFAULT 'whatever'::discount_birthday,
  date_from date DEFAULT NULL,
  date_to date DEFAULT NULL,
  has_phone discount_phone NOT NULL DEFAULT 'whatever'::discount_phone,
  phone_ends character(4) DEFAULT NULL,
  service bigint[],
  amount smallint,
  CONSTRAINT discount_pkey PRIMARY KEY (discount_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE discount
  OWNER TO "user";

EOT;

        $this->execute($sql);
    }

}